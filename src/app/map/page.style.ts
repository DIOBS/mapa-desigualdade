import { styled } from "styled-components";

const Container = styled.div`
  max-width: 100vw;
  display: flex;
  justify-content: space-between;
  padding: 2vw 2.22vw;
`;

export const Styles = {
    Container
}