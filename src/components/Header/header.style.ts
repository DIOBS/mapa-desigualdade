import { IoMdArrowDropdown } from "react-icons/io";
import { styled } from "styled-components";

const HeaderContainer = styled.div`
  width: 100vw;
  height: 5.91vw;
  display: flex;
  align-items: center;
  font-size: 1vw;
  padding: 0 2vw;
  background-color: brown;
`;

const NavBar = styled.div`
  display: flex;
  align-items: center;
  gap: 4vw;
`;

const ArrowIcon = styled(IoMdArrowDropdown)`
    font-size: 2vw;

    &:hover {
        cursor: pointer;
        scale: 110%;
    }
`

const NavItem = styled.div`
  display: flex;
  align-items: center;
  gap: 0.2vw;
  position: relative;
`;

const NavMenuItem = styled.div`
    position: absolute;
    left: 0;
    top: 2vw;
    display: flex;
    flex-direction: column;
    gap: 0.5vw;
`

export const Styles = {
  HeaderContainer,
  NavBar,
  ArrowIcon,
  NavItem,
  NavMenuItem
};
