"use client";

import Link from "next/link";
import { Styles } from "./header.style";
import { useState } from "react";

export default function Header() {
  const [toggleMapMenu, setMapMenu] = useState(false);

  return (
    <Styles.HeaderContainer>
      <Styles.NavBar>
        <Link href="/">Início</Link>
        <Link href="/">Introdução</Link>
        <Styles.NavItem>
          Mapa
          <Styles.ArrowIcon onClick={() => setMapMenu(!toggleMapMenu)} />
          {toggleMapMenu && (
            <Styles.NavMenuItem>
              <Link href="/map"><span onClick={() => setMapMenu(!toggleMapMenu)}>Mapa da desigualdade</span></Link>
              <Link href="/district">Distritos</Link>
              <Link href="/ranking">Ranking</Link>
            </Styles.NavMenuItem>
          )}
        </Styles.NavItem>
        <Link href="/">Informações adicionais</Link>
      </Styles.NavBar>
    </Styles.HeaderContainer>
  );
}
