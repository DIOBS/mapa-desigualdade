import { Styles } from "./mapContent.style";

type Props = {
  mapPath: string;
};

export default function MapContent({ mapPath }: Props) {
  return (
    <Styles.Container>
      <iframe
        id="iframe-mapa"
        src={mapPath}
        width="100%"
        height="100%"
      ></iframe>
    </Styles.Container>
  );
}
