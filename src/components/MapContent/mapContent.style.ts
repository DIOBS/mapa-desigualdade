import { styled } from "styled-components";

const Container = styled.div`
    width: 41.11vw;
    max-width: 41.11vw;
    height: 100vh;
`

export const Styles = {
    Container
}