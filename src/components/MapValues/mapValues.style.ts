import { styled } from "styled-components";

const Container = styled.div`
    width: 19.65vw;
    max-width: 19.65vw;
    height: 100vh;
`

export const Styles = {
    Container
}